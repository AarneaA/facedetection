
public class SkinTones {
	
	public boolean isSkinTone(int red, int green, int blue){
//		Can be skipped actually.
		float onePercent = (float) 2.55;
		float redBaseline = red / onePercent;
		float greenBaseline = green / onePercent;
		float blueBaseline = blue / onePercent;
		
		boolean result = false;
//		float redToGreen = redBaseline - greenBaseline;
//		float greenToBlue = greenBaseline - blueBaseline;
//		
//		float greenLowerBound = 42;
//		float greenUpperBound = 10;
//		float blueLowerBound = 8;
//		float blueUpperBound = 9;
		
//		Caucasian white balance average skintone.
//		if(redBaseline >= 76 && redBaseline <= 100 && greenBaseline <= 70 && greenBaseline >= 53 && blueBaseline <= 59 && blueBaseline >= 38){
//			return true;
//		}
//		if(redBaseline >= 46 && redBaseline <= 100 && greenBaseline <= 80 && greenBaseline >= 23 && blueBaseline <= 79 && blueBaseline >= 4){
//			return true;
//		}
		
		
//		RGB filtering for skintones
		
////		YCbCr filtering for skintones
//		
		float Cb = (float) (red*(-0.169) - 0.332 * green + 0.500 *blue);
		float Cr = (float) (0.500 * red - 0.419 * green - 0.081 * blue);
		
//		System.out.println(Cb + " " + Cr);
		
		if(Cb >= -22 && Cb <= 0 && Cr >= 13 && Cr <= 33){
			result = true;
		}
		
		if(redBaseline > 76 && greenBaseline > 50 && blueBaseline > 20 && Math.max(redBaseline, Math.max(blueBaseline,  greenBaseline)) - Math.min(redBaseline, Math.min(blueBaseline,  greenBaseline)) < 45 && Math.abs(redBaseline - greenBaseline) > 15 && redBaseline > greenBaseline && redBaseline > blueBaseline){
			result = true;
		}
		else{
//			result = false;
		}
//		77 <= Cb <= 127 ; 133 <=Cr <=173
//		Y = 0.299R + 0.587G + 0.114B
//		Cb = -0.169R - 0.332G + 0.500B
//		Cr = 0.500R - 0.419G - 0.081B
//		Dark skintone
//		else if(redBaseline >= 7 && redBaseline <= 100 && greenBaseline <= 58 && greenBaseline >= 1 && blueBaseline <= 54 && blueBaseline >= 1){
//			return true;
//		}
		return result;
//Below are some percentage values for skintones RGB
/*
 100% 87% 77%
 100% 86% 70%
 100% 90% 79%
 100% 89% 79%
 100% 83% 84%
 100% 87% 75%
 100% 81% 67%
 100% 68% 47%
 100% 85% 66%
 100% 80% 62%
 100% 70% 50%
 100% 85% 55%
 100% 82% 68%
 100% 79% 59%
 100% 74% 52%
 100% 77% 57%
 100% 76% 72%
 100% 66% 46%
 100% 70% 53%
 100% 73% 60%
 100% 65% 33%
 100% 61% 43%
 100  60% 32%
 100% 80% 63%
 100% 58% 39%
 100% 67% 59%
 100% 58% 47%
 100% 70% 64%
 100% 69% 53%
 100% 35% 0%
 100% 82% 65%
 100% .07% 0%
 100% 3% 0%
 100% 0% 0%
 100% 1% 1%
 100% 58% 51%
 100% 25% 0%
 100% 61% 59%
 100% 0% 1%
 100% 0% 0%
 100% 96% 96%
 */
	}
	
}
