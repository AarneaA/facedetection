
public class FindEdges {
	
	int edgeIntensity;

	private float getIntensity(int[][][] areaMedianMatrix, int i, int j){
		float initialIntesity = (float)(areaMedianMatrix[j][i][0] + areaMedianMatrix[j][i][1] + areaMedianMatrix[j][i][2]) / 3;
		return (float) Math.sqrt(initialIntesity);
	}
	
	private float getEdge(int[][][] areaMedianMatrix, int i, int j){
		return (float) Math.sqrt(Math.pow(getIntensity(areaMedianMatrix, i, j) - getIntensity(areaMedianMatrix, i+1, j+1), 2) + Math.pow(getIntensity(areaMedianMatrix, i + 1, j) - getIntensity(areaMedianMatrix, i, j+1), 2));
	}
	
	public void removeEdges(int[][][] areaMedianMatrix){
		for(int i = 1; i < areaMedianMatrix[0].length-1; i++){
			for(int j = 1 ; j < areaMedianMatrix.length-1; j++){
				float initialIntensity = (float)(areaMedianMatrix[j][i][0] + areaMedianMatrix[j][i][1] + areaMedianMatrix[j][i][2]) / 3;
				int newIntensity = (int) getEdge(areaMedianMatrix, i, j);
				if(initialIntensity * newIntensity > edgeIntensity){
					areaMedianMatrix[j][i][0] = 0;
					areaMedianMatrix[j][i][1] = 0;
					areaMedianMatrix[j][i][2] = 0;
				}
			}
		}
	}

	public int getEdgeIntensity() {
		return edgeIntensity;
	}

	public void setEdgeIntensity(int edgeIntensity) {
		this.edgeIntensity = edgeIntensity;
	}
	
	
	
}
