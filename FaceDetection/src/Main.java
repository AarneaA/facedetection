import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;


public class Main {

	public static void main(String[] args) {
		
		  ImageToRGB imageToRGB = new ImageToRGB();
		  AreaMatrix areaMatrix = new AreaMatrix();
		  FindEdges findedges = new FindEdges();
	      CleanUp cleanup = new CleanUp();
	      DetectFaces detectFace = new DetectFaces();
	      
	      //Parameters
	      
	      //May be between 3 - picture size.
		  areaMatrix.setAreaSize(4);
		  
		  //May be between 1 - getAreaSize()*getAreaSize().
		  areaMatrix.setPixelsPerArea(areaMatrix.getAreaSize()*areaMatrix.getAreaSize()/4);
		  
		  //May be between 0 - picture size/getAreaSize().
	      cleanup.setMinimumSkinPatch(400);
	      
	      //May be between 0 - 100.
	      detectFace.setSkinPecentageForDetection(45);
	      
	      //Roberts Gross edge detection. May be between 0-255.
	      findedges.setEdgeIntensity(255);
	      
		  BufferedImage imageToSearch = null;
		  
		  try {
			  imageToSearch = ImageIO.read(new File("family.jpg"));
		  } catch (IOException e) {
			  e.printStackTrace();
		  }
		  
		  int[][][] RGBMatrix = imageToRGB.imageToRGBMatrix(imageToSearch);
		  
		  int[][][] areaMedianMatrix = areaMatrix.makeAreaMedianMatrix(RGBMatrix);
		  
		  findedges.removeEdges(areaMedianMatrix);
		  
		  SkinTones skinTones = new SkinTones();
	      for (int h = 0; h < areaMedianMatrix[0].length; h++) {
	    	  for (int w = 0; w < areaMedianMatrix.length; w++) {
	    		  if(skinTones.isSkinTone(areaMedianMatrix[w][h][0], areaMedianMatrix[w][h][1], areaMedianMatrix[w][h][2])){
	    			  areaMedianMatrix[w][h][0] = 255;
	    			  areaMedianMatrix[w][h][1] = 255;
	    			  areaMedianMatrix[w][h][2] = 255;
	    		  }
	    		  else{
	    			  areaMedianMatrix[w][h][0] = 0;
	    			  areaMedianMatrix[w][h][1] = 0;
	    			  areaMedianMatrix[w][h][2] = 0;
	    		  }
	    	  }
	      }
	      
	      ArrayList<int[]> faceOutline = new ArrayList<int[]>();
	      cleanup.removeSmallWhites(areaMedianMatrix, faceOutline, detectFace);
	      
//	      //detectFace.paintFaceOutlines(areaMedianMatrix, faceOutline, areaMatrix.getAreaSize());
//		  
//		  imageToRGB.RGBMatrixToImage(areaMedianMatrix);
	      
	      detectFace.paintFaceOutlines(RGBMatrix, faceOutline, areaMatrix.getAreaSize());
		  
		  imageToRGB.RGBMatrixToImage(RGBMatrix);
		  
	  }

}
