import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class ImageToRGB {
	
	public int[][][] imageToRGBMatrix(BufferedImage image){
		int imageHeight = image.getHeight();
		int imageWidth = image.getWidth();
		int[][][] RPGMatrix = new int[imageWidth][imageHeight][3];
		
		for(int h = 0; h < imageHeight; h++){
			for(int w = 0; w < imageWidth; w++){
				Color color = new Color(image.getRGB(w, h));
				int[] RBGValues = {color.getRed(), color.getGreen(), color.getBlue()};
				RPGMatrix[w][h] = RBGValues;
			}
		}
		
		return RPGMatrix;
	}

	public void RGBMatrixToImage(int[][][] RGBMatrix){
		int imageHeight = RGBMatrix[0].length;
		int imageWidth = RGBMatrix.length;
		
	    try {
	        BufferedImage bufferedImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
	        for (int h = 0; h < imageHeight; h++) {
	        	for (int w = 0; w < imageWidth; w++) {
	        		int pixel = (RGBMatrix[w][h][0] << 16) | (RGBMatrix[w][h][1] << 8) | (RGBMatrix[w][h][2]);
	        		bufferedImage.setRGB(w, h, pixel);
	        	}
	        }
	        File outputfile = new File("test.png");
	        ImageIO.write(bufferedImage, "png", outputfile);
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	    
	}
}

