import java.util.ArrayList;


public class DetectFaces {
	
	int skinPecentageForDetection;

	boolean detectFaces(int segmentSize, int minHeight, int maxHeight, int minWidth, int maxWidth){
		int height = maxHeight-minHeight;
		int width = maxWidth-minWidth;
		
		if(height/2 > width){
			return false;
		}
		if(width/2 > height){
			return false;
		}
		
		int areaSize = (height)*(width);
		int whiteAreaPercentage = 100*segmentSize/areaSize;
		
		if(whiteAreaPercentage > getSkinPecentageForDetection()){
			return true;
		}
		else{
			return false;
		}
		
		
	}

	public void paintFaceOutlines(int[][][] areaMedianMatrix, ArrayList<int[]> faceOutlines, int areaSize) {
		int[] visited = new int[faceOutlines.size()];
		ArrayList<int[]> faces = new ArrayList<int[]>();
		for(int i = 0; i < faceOutlines.size(); i++){
			if(visited[i] == 0){
				boolean change = false;
				visited[i] = 1;
				int minH = faceOutlines.get(i)[0];
				int maxH = faceOutlines.get(i)[1];
				int minW = faceOutlines.get(i)[2];
				int maxW = faceOutlines.get(i)[3];
				for(int j = i; j < faceOutlines.size(); j++){
					if(visited[j] == 0){
						int minH2 = faceOutlines.get(j)[0];
						int maxH2 = faceOutlines.get(j)[1];
						int minW2 = faceOutlines.get(j)[2];
						int maxW2 = faceOutlines.get(j)[3];
						if(minH < maxH2 && maxH > minH2 && minW < maxW2 && maxW > minW2){
							change = true;
							visited[j] = 1;
							int[] newBorder = {Math.min(minH, minH2), Math.max(maxH, maxH2), Math.min(minW, minW2), Math.max(maxW, maxW2)};
							faces.add(newBorder);
							break;
						}
					}
				}
				if(!change){
					int[] newBorder = {minH, maxH, minW, maxW};
					faces.add(newBorder);
				}
			}
		}
		
		for(int[] outlines: faces){
			for(int h = outlines[0]*areaSize-2; h <= outlines[1]*areaSize+2; h++){
				for(int w = outlines[2]*areaSize-2; w <= outlines[3]*areaSize+2; w++){
					boolean outOfH = true;
					boolean outOfW = true;
					if(h < 0){
						outOfH = false;
					}
					if(w < 0){
						outOfW = false;
					}
					if(outOfH&& outOfW && ((h <= outlines[0]*areaSize && h >= outlines[0]*areaSize-2) || (h >= outlines[1]*areaSize && h <= outlines[1]*areaSize+2))){
		    			  areaMedianMatrix[w][h][0] = 255;
		    			  areaMedianMatrix[w][h][1] = 0;
		    			  areaMedianMatrix[w][h][2] = 0;
					}
					else if(outOfW && outOfH && ((w <= outlines[2]*areaSize && w >= outlines[2]*areaSize-2) || (w >= outlines[3]*areaSize && w <= outlines[3]*areaSize+2))){
		    			  areaMedianMatrix[w][h][0] = 255;
		    			  areaMedianMatrix[w][h][1] = 0;
		    			  areaMedianMatrix[w][h][2] = 0;
					}
				}
			}
		}
	}

	public int getSkinPecentageForDetection() {
		return skinPecentageForDetection;
	}

	public void setSkinPecentageForDetection(int skinPecentageForDetection) {
		this.skinPecentageForDetection = skinPecentageForDetection;
	}
	
	
}
