import java.util.ArrayList;
import java.util.Arrays;


public class CleanUp {
	
	int minHeight;
	int maxHeight;
	int minWidth;
	int maxWidth;
	
	boolean isInBorder;
	
	int minimumSkinPatch;

	private void recurseWhiteAreas(int[][] visited, int[][][] areaMedianMatrix, int currenti, int currentj, ArrayList<int[]> segment ){
		if(currenti > maxHeight){
			maxHeight = currenti;
		}
		if(currentj > maxWidth){
			maxWidth = currentj;
		}
		if(currenti < minHeight){
			minHeight = currenti;
		}
		if(currentj < minWidth){
			minWidth = currentj;
		}
		visited[currentj][currenti] = 1;
		int[] addedPiece = {currentj, currenti};
		segment.add(addedPiece);
		if(areaMedianMatrix[currentj].length > currenti + 1){
			if((visited[currentj][currenti + 1] == 0 && areaMedianMatrix[currentj][currenti + 1][0] == 255)){
				recurseWhiteAreas(visited, areaMedianMatrix, currenti + 1, currentj, segment);
			}
		}
		if(areaMedianMatrix.length > currentj + 1){
			if((visited[currentj + 1][currenti] == 0 &&  areaMedianMatrix[currentj + 1][currenti][0] == 255)){
				recurseWhiteAreas(visited, areaMedianMatrix, currenti, currentj + 1, segment);
			}
		}
		if(0 <= currenti - 1){
			if(visited[currentj][currenti - 1] == 0 &&  areaMedianMatrix[currentj][currenti - 1][0] == 255){
				recurseWhiteAreas(visited, areaMedianMatrix, currenti - 1, currentj, segment);
			}
		}
		if(0 <= currentj - 1){
			if( visited[currentj - 1][currenti] == 0 &&  areaMedianMatrix[currentj - 1][currenti][0] == 255){
				recurseWhiteAreas(visited, areaMedianMatrix, currenti, currentj - 1, segment);
			}
		}
	}
	
	public void removeSmallWhites(int[][][] areaMedianMatrix, ArrayList<int[]> faceOutline, DetectFaces detectFace){
//		TODO: find first white square
//		TODO: discover neighbors, write up found whites until queue empty
		int[][] visited = new int[areaMedianMatrix.length][areaMedianMatrix[0].length];
		for(int i = 0; i < areaMedianMatrix[0].length; i++){
			for(int j = 0 ; j < areaMedianMatrix.length; j++){
//				if not visited
				if(visited[j][i] == 0 && areaMedianMatrix[j][i][0] == 255){
					minHeight = Integer.MAX_VALUE;
					maxHeight = 0;
					minWidth = Integer.MAX_VALUE;
					maxWidth = 0;
					ArrayList<int[]> segment = new ArrayList<int[]>();
					recurseWhiteAreas(visited, areaMedianMatrix, i, j, segment);
					if(segment.size() < minimumSkinPatch){
						for(int[] square: segment){
							areaMedianMatrix[square[0]][square[1]][0] = 0;
							areaMedianMatrix[square[0]][square[1]][1] = 0;
							areaMedianMatrix[square[0]][square[1]][2] = 0;
						}
					}
					else{
						for(int h = minHeight; h <= maxHeight; h++){
							for(int w = minWidth; w <= maxWidth; w++){
								isInBorder = false;
								ArrayList<int[]> segmentBlack = new ArrayList<int[]>();
								if(visited[w][h] == 0 && areaMedianMatrix[w][h][0] == 0){
									recurseBlackAreas(visited, areaMedianMatrix, h, w, segmentBlack);
									if(!isInBorder){
										for(int[] square: segmentBlack){
											areaMedianMatrix[square[0]][square[1]][0] = 255;
											areaMedianMatrix[square[0]][square[1]][1] = 255;
											areaMedianMatrix[square[0]][square[1]][2] = 255;
											int[] coordinates = {square[0], square[1]};
											segment.add(coordinates);
										}
									}
								}
								else if(visited[w][h] == 0 && areaMedianMatrix[w][h][0] == 255){
									int[] coordinates = {w, h};
									boolean w1 = false;
									boolean w2 = false;
									boolean h1 = false;
									boolean h2 = false;
									if(w-1 >= 0){
										w1 = true;
									}
									if(h-1 >= 0){
										h1 = true;
									}
									if(w+1 < areaMedianMatrix.length){
										w2 = true;
									}
									if(h+1 < areaMedianMatrix[0].length){
										h2 = true;
									}
									
									if(w1 && visited[w-1][h] == 1 && areaMedianMatrix[w-1][h][0] == 255){
										segment.add(coordinates);
										visited[w][h] = 1;
									}
									else if(h1 && visited[w][h-1] == 1 && areaMedianMatrix[w][h-1][0] == 255){
										segment.add(coordinates);
										visited[w][h] = 1;
									}
									else if(w2 && visited[w+1][h] == 1 && areaMedianMatrix[w+1][h][0] == 255){
										segment.add(coordinates);
										visited[w][h] = 1;
									}
									else if(h2 && visited[w][h+1] == 1 && areaMedianMatrix[w][h+1][0] == 255){
										segment.add(coordinates);
										visited[w][h] = 1;
									}
								}
							}
						}
						
						boolean isFace = detectFace.detectFaces(segment.size(), minHeight, maxHeight, minWidth, maxWidth);
						if(isFace){
							int[] outlines = {minHeight, maxHeight, minWidth, maxWidth};
							faceOutline.add(outlines);
						}
					}
				}
			}
		}
	}
	
	private void recurseBlackAreas(int[][] visited, int[][][] areaMedianMatrix, int currenti, int currentj, ArrayList<int[]> segmentBlack){
		visited[currentj][currenti] = 1;
		int[] addedPiece = {currentj, currenti};
		segmentBlack.add(addedPiece);
		
		if(currenti == maxHeight-1 || currentj == maxWidth-1 || currenti == minHeight || currentj == minWidth){
			isInBorder = true;
		}
		
		if(areaMedianMatrix[currentj].length > currenti + 1 && currenti + 1 < maxHeight ){
			if((visited[currentj][currenti + 1] == 0 && areaMedianMatrix[currentj][currenti + 1][0] == 0)){
				recurseBlackAreas(visited, areaMedianMatrix, currenti + 1, currentj, segmentBlack);
			}
		}
		if(areaMedianMatrix.length > currentj + 1 && currentj + 1 < maxWidth){
			if((visited[currentj + 1][currenti] == 0 &&  areaMedianMatrix[currentj + 1][currenti][0] == 0)){
				recurseBlackAreas(visited, areaMedianMatrix, currenti, currentj + 1, segmentBlack);
			}
		}
		if(0 <= currenti - 1 && currenti - 1 >= minHeight){
			if(visited[currentj][currenti - 1] == 0 &&  areaMedianMatrix[currentj][currenti - 1][0] == 0){
				recurseBlackAreas(visited, areaMedianMatrix, currenti - 1, currentj, segmentBlack);
			}
		}
		if(0 <= currentj - 1 && currentj - 1 >= minWidth){
			if(visited[currentj - 1][currenti] == 0 &&  areaMedianMatrix[currentj - 1][currenti][0] == 0){
				recurseBlackAreas(visited, areaMedianMatrix, currenti, currentj - 1, segmentBlack);
			}
		}
		
		if(currenti == maxHeight || currentj == maxWidth || currenti == minHeight || currentj == minWidth){
			isInBorder = true;
		}
	}

	public int getMinimumSkinPatch() {
		return minimumSkinPatch;
	}

	public void setMinimumSkinPatch(int minimumSkinPatch) {
		this.minimumSkinPatch = minimumSkinPatch;
	}
	
	
}
