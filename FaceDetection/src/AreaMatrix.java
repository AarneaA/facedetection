import java.util.ArrayList;
import java.util.Arrays;


public class AreaMatrix {
	
	int areaSize;
	int pixelsPerArea;

	public int[][][] makeAreaMedianMatrix(int[][][] RGBMatrix){
		
		int[][][] areaMatrix = new int[RGBMatrix.length/areaSize][RGBMatrix[0].length/areaSize][3];
		
        for (int h = 0; h < areaMatrix[0].length; h++) {
        	for (int w = 0; w < areaMatrix.length; w++) {
        		int[][] randomPixelPositionMatrix = randomPixelPosition(w, h, pixelsPerArea, areaSize);
        		int[] redPixelList = new int[pixelsPerArea];
        		int[] greenPixelList = new int[pixelsPerArea];
        		int[] bluePixelList = new int[pixelsPerArea];
        		
        		for(int row = 0; row < pixelsPerArea; row++){
        			redPixelList[row] = RGBMatrix[randomPixelPositionMatrix[row][0]][randomPixelPositionMatrix[row][1]][0];
        			greenPixelList[row] = RGBMatrix[randomPixelPositionMatrix[row][0]][randomPixelPositionMatrix[row][1]][1];
        			bluePixelList[row] = RGBMatrix[randomPixelPositionMatrix[row][0]][randomPixelPositionMatrix[row][1]][2];
        		}
        		Arrays.sort(redPixelList);
        		Arrays.sort(greenPixelList);
        		Arrays.sort(bluePixelList);
        		int redPixel = (redPixelList[redPixelList.length/2]+redPixelList[(redPixelList.length/2)+1])/2;
        		int greenPixel = (greenPixelList[greenPixelList.length/2]+greenPixelList[(greenPixelList.length/2)+1])/2;
        		int bluePixel = (bluePixelList[bluePixelList.length/2]+bluePixelList[(bluePixelList.length/2)+1])/2;
        		
        		areaMatrix[w][h][0] = redPixel;
        		areaMatrix[w][h][1] = greenPixel;
        		areaMatrix[w][h][2] = bluePixel;
        	}
        }
		
		return areaMatrix;
		
	}

	public int[][] randomPixelPosition(int areaRowPosition, int areaColumnPosition, int pixelsPerArea, int areaSize){
		int[][] randomPixelPositionMatrix = new int[pixelsPerArea][2];
		
		for(int row = 0; row < pixelsPerArea;row++){
	        int pixelRowPosition = (int) (areaSize * Math.random())+areaRowPosition*areaSize;
	        int pixelColumnPosition = (int) (areaSize * Math.random())+areaColumnPosition*areaSize;
	        randomPixelPositionMatrix[row][0] = pixelRowPosition;
	        randomPixelPositionMatrix[row][1] = pixelColumnPosition;
	        
		}

        return randomPixelPositionMatrix;
	}
	
	public int getAreaSize() {
		return areaSize;
	}

	public void setAreaSize(int areaSize) {
		this.areaSize = areaSize;
	}
	
	public int getPixelsPerArea() {
		return pixelsPerArea;
	}

	public void setPixelsPerArea(int pixelsPerArea) {
		this.pixelsPerArea = pixelsPerArea;
	}
}
