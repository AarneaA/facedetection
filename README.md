# README #



### Face Detection ###

This is a repository for our Algorithmics course project.

We use probabilistic algorithms for speeding up the face detection algorithm in order to detect faces on pictures based on skintones.

###Minimizing input###
The input is an image and we are dealing with the pixels of said image (n = height * width). We convert the input image into a smaller one, where a pixel represents the median color value of random pixels within a predetermined area. 
By minimizing the input we ensure that upcoming operations do not have to be repeated for every pixel, since we have deemed it unnecessary. 

###Finding edges###
Since our solution works based on detecting skintones it is important to separate different densely placed skintone objects from eachother, for example faces that are covered by hands or another person. For separating these skin objects we utilize Roberts Cross Operator, which assesses the gradient differences, between diagonally placed pixels.
The pixels that are determined as edges are represented by black pixels, because in a later step black pixels represent every pixel without a detected skintone.

###Finding skintones###
The next step is to travel the pixels and determine, whether or not the pixel represents a skintone. To do this we have implemented a RGB filter, which checks if the red, green and blue values are within certain ranges and with certain dependencies between themselves. The RGB filter by itself is too dependent on the lighting of the image, thus we also implemented a YCbCr filter that takes into account the color components, ignoring lighting.
The pixels which contain skintones are from now represented as white and everything else as black pixels.

###Removing noise###
At this point we have a representation of the original image consisting of white and black pixels. We use DFS to determine separate white segments and remove them, if they are smaller than a certain value. We remove black noise by similarly DFS’ing black pixels which fall into a rectangle shaped area and are surrounded by white pixels.
Next we remove white areas, that are too long in shape to be human faces, we do this by calculating the percentage of white pixels that fall within a rectangular area, tight around the white area, and removing them if they fall below a determined threshold.

###Highlighting faces###
By this last step, all the areas, that are marked as skintones, we count as faces. All that needs to be done, is recalculate the size of the rectangles around white areas to fit around their respective faces in the original image and making the pixels red.
Future work
Our solution comes with a handful of variables that require training to find optimal values. (The variables: limits for the RGB and YCbCr filters, Roberts Cross intensity, skin percentage per segment, removable noise size.
Face detection can be used as a basis for training face recognition software (using the cropped output of a detector as the inputs for the recognizer).
